
//Frequency of the oscillator, used for delays
#define _XTAL_FREQ              1000000

//Number of characters in a line of the LCD
#define LCD_WIDTH               16

//LCD Enable bit (use preferably LAT registers to avoid Read-Modify-Write glitches)
#define	LCD_E_BIT               LATDbits.LD6

//LCD RS bit (use preferably LAT registers to avoid Read-Modify-Write glitches)
#define	LCD_RS_BIT              LATDbits.LD4

//LCD data bus register (use preferably LAT registers to avoid Read-Modify-Write glitches)
#define LCD_DATA_REGISTER       LATD

//If defined, the LCD data bus is connected through pins 3 to 0 of data port
#define LCD_DATA_REGISTER_LOWER_4BITS

//If defined, the LCD data bus is connected through pin 7 to 4 of data port
//#define LCD_DATA_REGISTER_UPPER_4BITS


//If defined, the library will change others bits on the data port
//It is usually NOT a good thing to enable it
//#define LCD_CAN_CHANGE_OTHER_BITS