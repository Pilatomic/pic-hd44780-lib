#include "hd44780.h"
#include "../config.h"

#define LCDclk()LCD_E_BIT=1;__delay_us(50);LCD_E_BIT=0;__delay_us(50)


#if !defined(LCD_DATA_REGISTER_LOWER_4BITS) && !defined(LCD_DATA_REGISTER_UPPER_4BITS)
    #error LCD_PORT_LOWER_4BITS or LCD_PORT_UPPER_4BITS must be defined

#elif defined(LCD_DATA_REGISTER_LOWER_4BITS) && defined(LCD_DATA_REGISTER_UPPER_4BITS)
    #error LCD_PORT_LOWER_4BITS and LCD_PORT_UPPER_4BITS cannot be defined at the same time

#endif


#ifdef LCD_DATA_REGISTER_LOWER_4BITS
    #ifdef LCD_CAN_CHANGE_OTHER_BITS
        #define LCD_WRITE_LOWER_BITS(x) LCD_DATA_REGISTER= ( x & 0x0F ) ;
        #define LCD_WRITE_UPPER_BITS(x) LCD_DATA_REGISTER= (( x >> 4 ) & 0x0F ) ;
    #else
        #define LCD_WRITE_LOWER_BITS(x) LCD_DATA_REGISTER= ( LCD_DATA_REGISTER & 0xF0 ) | ( x & 0x0F ) ;
        #define LCD_WRITE_UPPER_BITS(x) LCD_DATA_REGISTER= ( LCD_DATA_REGISTER & 0xF0 ) | (( x >> 4 ) & 0x0F ) ;        
    #endif
#endif

#ifdef LCD_DATA_REGISTER_UPPER_4BITS
    #ifdef LCD_CAN_CHANGE_OTHER_BITS
        #define LCD_WRITE_LOWER_BITS(x) LCD_DATA_REGISTER= (( x << 4 ) & 0xF0 ) ;
        #define LCD_WRITE_UPPER_BITS(x) LCD_DATA_REGISTER= ( x & 0xF0 ) ;     
    #else
        #define LCD_WRITE_LOWER_BITS(x) LCD_DATA_REGISTER= ( LCD_DATA_REGISTER & 0x0F ) | (( x << 4 ) & 0xF0 ) ;
        #define LCD_WRITE_UPPER_BITS(x) LCD_DATA_REGISTER= ( LCD_DATA_REGISTER & 0x0F ) | ( x & 0xF0 ) ;
    #endif
#endif

void LCDinit(uint8_t param){
	//SOFTWARE RESET
	LCD_RS_BIT=0;

    LCD_WRITE_LOWER_BITS(0x03);
    LCDclk();
	__delay_ms(5);
    LCD_WRITE_LOWER_BITS(0x03);
    LCDclk();
	__delay_us(200);
    LCD_WRITE_LOWER_BITS(0x03);
    LCDclk();
    
    //4-bits mode
    LCD_WRITE_LOWER_BITS(0x02);
    
    //init
    LCDclk();
	LCDwrite(LCD_WRITE_CMD,0x2C);// > 1 line, 4 bit
	LCDwrite(LCD_WRITE_CMD,0b00001100|(param&0b00000011));//LCD on, cursor and blinking set by param
	LCDwrite(LCD_WRITE_CMD,0x14);//move cursor right, no shifting
	LCDwrite(LCD_WRITE_CMD,LCD_CMD_CLEAR);
}	

void LCDwrite(uint8_t type, uint8_t c){
	LCD_WRITE_UPPER_BITS(c);
	LCD_RS_BIT=type;	
    LCDclk();
	LCD_WRITE_LOWER_BITS(c);;
	LCD_RS_BIT=type;	
    LCDclk();
}

void LCDprint(char p[]){
	while(*p){
		LCDwrite(LCD_WRITE_DATA,*p);
		p++;
	}
}	

void LCDprintConst(const char p[]){
	while(*p){
		LCDwrite(LCD_WRITE_DATA,*p);
		p++;
	}
}	

void LCDpos(uint8_t x, uint8_t y){
	if((x & 0x01) == 0x01) y+=0x40;
	if((x & 0x02) == 0x02) y+=LCD_WIDTH;
	LCDwrite(LCD_WRITE_CMD,0x80 + y);
}

void LCDvalue8fp(uint8_t v, uint8_t fp){
	LCDwrite(LCD_WRITE_CMD,0x04);					//now cursor move left
    LCDwrite(LCD_WRITE_DATA,0x30+v%10);
    for(uint8_t i = 1; i<=fp || v>=10 ; i++){
        if(i == fp )LCDwrite(LCD_WRITE_DATA,'.');
        v/=10;
        LCDwrite(LCD_WRITE_DATA,0x30+v%10);
    }
	LCDwrite(LCD_WRITE_CMD,6);					//restore normal cursor behavior
}

void LCDvalue16fp(uint16_t v, uint8_t fp){
	LCDwrite(LCD_WRITE_CMD,0x04);					//now cursor move left
    LCDwrite(LCD_WRITE_DATA,0x30+v%10);
    for(uint8_t i = 1; i<=fp || v>=10 ; i++){
        if(i == fp )LCDwrite(LCD_WRITE_DATA,'.');
        v/=10;
        LCDwrite(LCD_WRITE_DATA,0x30+v%10);
    }
	LCDwrite(LCD_WRITE_CMD,6);					//restore normal cursor behavior
}

void LCDsetCustomChar(uint8_t carnumber, uint8_t * car){
	LCDwrite(LCD_WRITE_CMD,0x40|((carnumber&0b00000111)<<3));
	for(char i=0;i<8;i++){
		LCDwrite(LCD_WRITE_DATA,car[i]);
	}
}

void LCDsetCustomCharConst(uint8_t carnumber, const uint8_t * car){
	LCDwrite(LCD_WRITE_CMD,0x40|((carnumber&0b00000111)<<3));
	for(char i=0;i<8;i++){
		LCDwrite(LCD_WRITE_DATA,car[i]);
	}
}

void LCDsetupProgressBar(void){
    const char progressChar0[8] = LCD_PROGRESS_CHAR_0;
    LCDsetCustomCharConst(0,progressChar0);
    const char progressChar1[8] = LCD_PROGRESS_CHAR_1;
    LCDsetCustomCharConst(1,progressChar1);
    const char progressChar2[8] = LCD_PROGRESS_CHAR_2;
    LCDsetCustomCharConst(2,progressChar2);
    const char progressChar3[8] = LCD_PROGRESS_CHAR_3;
    LCDsetCustomCharConst(3,progressChar3);
    const char progressChar4[8] = LCD_PROGRESS_CHAR_4;
    LCDsetCustomCharConst(4,progressChar4);
    const char progressChar5[8] = LCD_PROGRESS_CHAR_5;
    LCDsetCustomCharConst(5,progressChar5);
    LCDhome();
}

void LCDprogressBar(uint8_t line, uint8_t value, uint8_t total){
    uint8_t progressValue = (uint16_t)value*LCD_WIDTH*5/total;
    LCDpos(line,0);
    for(uint8_t i = 0 ; i < LCD_WIDTH ; i++){
        uint8_t currentValue = (progressValue > 5) ? 5 : progressValue;
        LCDuseCustomChar(currentValue);
        progressValue-=currentValue;
    }
}