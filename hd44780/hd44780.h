#include <xc.h>
#include <stdint.h>

//##############################################################################
//##############                                                  ##############
//##############               PROGRESS BAR SYMBOLS               ##############
//############## Edit this to change the look of the progress bar ##############
//##############                                                  ##############
//##############################################################################

#define LCD_PROGRESS_CHAR_0         {0b00010101,0b00001010,0b00010101,0b00001010,0b00010101,0b00001010,0b00010101,0b00001010}
#define LCD_PROGRESS_CHAR_1         {0b00010101,0b00011010,0b00010101,0b00011010,0b00010101,0b00011010,0b00010101,0b00011010}
#define LCD_PROGRESS_CHAR_2         {0b00011101,0b00011010,0b00011101,0b00011010,0b00011101,0b00011010,0b00011101,0b00011010}
#define LCD_PROGRESS_CHAR_3         {0b00011101,0b00011110,0b00011101,0b00011110,0b00011101,0b00011110,0b00011101,0b00011110}
#define LCD_PROGRESS_CHAR_4         {0b00011111,0b00011110,0b00011111,0b00011110,0b00011111,0b00011110,0b00011111,0b00011110}
#define LCD_PROGRESS_CHAR_5         {0b00011111,0b00011111,0b00011111,0b00011111,0b00011111,0b00011111,0b00011111,0b00011111}


//##############################################################################
//##############                                                  ##############
//##############                   DEFINITIONS                    ##############
//##############                 For internal use                 ##############
//##############                                                  ##############
//##############################################################################

//For LCDinit
#define LCD_INIT_CURSOR_DISABLED    0x00
#define LCD_INIT_CURSOR_ENABLED     0x02
#define LCD_INIT_CURSOR_BLINKING    0x01

//For LCDwrite
#define LCD_WRITE_CMD               0
#define LCD_WRITE_DATA              1

//Usefull commands
#define LCD_CMD_CLEAR               0x01        //Takes 1.52ms to execute. Leave 2ms delay before next command
#define LCD_CMD_HOME                0x02        //Takes 1.52ms to execute. Leave 2ms delay before next command

//##############################################################################
//##############                                                  ##############
//##############                      MACROS                      ##############
//##############                                                  ##############
//##############################################################################

#define LCDhome()           LCDwrite(LCD_WRITE_CMD,LCD_CMD_HOME);  __delay_ms(2)
//Set the cursor to the top left of the display

#define LCDclear()          LCDwrite(LCD_WRITE_CMD,LCD_CMD_CLEAR); __delay_ms(2)
//Clear the display, and set the cursor to the top left of the display

#define LCDuseCustomChar(x) LCDwrite(LCD_WRITE_DATA,x & 0x7);
//Displays the custom char in the bank X ( 0 to 7)

#define LCDvalue8(v)        LCDvalue8fp(v, 0)
//Displays the value of an 8 bit unsigned number

#define LCDvalue16(v)       LCDvalue16fp(v, 0)
//Displays the value of an 16 bit unsigned number


//##############################################################################
//##############                                                  ##############
//##############                    FUNCTIONS                     ##############
//##############                                                  ##############
//##############################################################################

void LCDinit(uint8_t param);
//Soft Reset & Init
//Call this function first before attempting to use anything related to the LCD
//Before calling this function, give the time to the LCD to start-up ...
// ... a 50ms delay should be enough
//Last command takes 1.52 ms to execute. Leave 2 ms delay before next command

void LCDprint(char string[]);
void LCDprintConst(const char string[]);
//Displays a string to the LCD
//With LCDprint, the string must be declared before. Example : 
// char test[] = "The cake is a lie";
// LCDprint(test);
//With LCDprintConst, strings can be declared in the function call. Example :
// LCDprintConst(""The cake is a lie");

void LCDvalue8fp(uint8_t value, uint8_t decimale);
void LCDvalue16fp(uint16_t value, uint8_t decimale);
//Displays 8 bits and 16 bits unsigned integer (uint8_t, unsigned char)
//decimale allows for decimal fixed point representation. It shows the value divided by 10^decimale. 0 to ignore
//Ex : with value = 11235 : decimale = 0 shows 11235, decimale = 1 shows 1123.5, decimale = 2 show 112.35, etc
//Before calling this function, place the cursor at the right of the position you want to display the value

void LCDpos(uint8_t line, uint8_t column);
//Set the cursor position 
// (0,0) is first line, first column

void LCDsetCustomChar(uint8_t carnumber, uint8_t * car);
void LCDsetCustomCharConst(uint8_t carnumber, const uint8_t * car);
//Write user defined characters to CGRAM. 
//Must be followed by another LCDsetCustomChar, or LCDpos, HOME or CLEAR command.
// charnumber : the data bank to stock the character : 0 to 7
// car : char array. Each char represent 1 line ( 5 pixels ), from top to bottom 
// There is a total of 8 lines. Use the the last line is not advised because of the cursor
// Ex :  char newCar [8] = {0b00011111,0b00010001,0b00010001,0b00010001,0b00010001,0b00010001,0b00011111,0x00};
//You can use customs characters with LCDwrite(LCD_WRITE_DATA, characterNumber),...
// ... or with convenient macro LCDuseCustomChar(x))

void LCDwrite(uint8_t type ,uint8_t data);
//send data (LCD_DATA) or command (LCD_COMMAND) to the display
//Available for advanced usage of the display ( for example set cursors without calling LCDinit)
//You are not supposed to call this function, unless you know what you are doing

void LCDsetupProgressBar(void);
//Makes a progress bar available through LCDprogressBar() function
//Call it at the beginning of the program

void LCDprogressBar(uint8_t line, uint8_t value, uint8_t total);
//Displays a progress bar on a full line, showing value/total ratio
//Uses customs chars banks 0 to 5