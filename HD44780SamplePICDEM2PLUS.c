/* 
 * File:   main.c
 * Author: Pila
 *
 * Created on 5 mai 2015, 21:22
 */

#include <stdint.h>
#include "config.h"
#include "hd44780/hd44780.h"

/* Example of usage of the HD44780 lib on a PICDEM2PLUS board with PIC18F4520
 * The LCD is connected via PORTD : 
 * DATA via RD3 to RD0 
 * RS via RD4
 * E via RD6
 * R/W via RD5   //Not needed by the lib, always set to 0. Wire it directly to ground to save a pin
 * LCD POWER ON via RD7 // On the PICDEM2PLUS board, this pins controls power supply to the LCD
 */

// CONFIG1H
#pragma config OSC = INTIO67    // Oscillator Selection bits (Internal oscillator block, port function on RA6 and RA7)
#pragma config FCMEN = OFF      // Fail-Safe Clock Monitor Enable bit (Fail-Safe Clock Monitor disabled)
#pragma config IESO = OFF       // Internal/External Oscillator Switchover bit (Oscillator Switchover mode disabled)

// CONFIG2L
#pragma config PWRT = OFF       // Power-up Timer Enable bit (PWRT disabled)
#pragma config BOREN = OFF      // Brown-out Reset Enable bits (Brown-out Reset disabled in hardware and software)
#pragma config BORV = 3         // Brown Out Reset Voltage bits (Minimum setting)

// CONFIG2H
#pragma config WDT = OFF        // Watchdog Timer Enable bit (WDT disabled (control is placed on the SWDTEN bit))
#pragma config WDTPS = 32768    // Watchdog Timer Postscale Select bits (1:32768)

// CONFIG3H
#pragma config CCP2MX = PORTC   // CCP2 MUX bit (CCP2 input/output is multiplexed with RC1)
#pragma config PBADEN = OFF     // PORTB A/D Enable bit (PORTB<4:0> pins are configured as digital I/O on Reset)
#pragma config LPT1OSC = OFF    // Low-Power Timer1 Oscillator Enable bit (Timer1 configured for higher power operation)
#pragma config MCLRE = ON       // MCLR Pin Enable bit (MCLR pin enabled; RE3 input pin disabled)

// CONFIG4L
#pragma config STVREN = ON      // Stack Full/Underflow Reset Enable bit (Stack full/underflow will cause Reset)
#pragma config LVP = OFF        // Single-Supply ICSP Enable bit (Single-Supply ICSP disabled)
#pragma config XINST = OFF      // Extended Instruction Set Enable bit (Instruction set extension and Indexed Addressing mode disabled (Legacy mode))

// CONFIG5L
#pragma config CP0 = OFF        // Code Protection bit (Block 0 (000800-001FFFh) not code-protected)
#pragma config CP1 = OFF        // Code Protection bit (Block 1 (002000-003FFFh) not code-protected)
#pragma config CP2 = OFF        // Code Protection bit (Block 2 (004000-005FFFh) not code-protected)
#pragma config CP3 = OFF        // Code Protection bit (Block 3 (006000-007FFFh) not code-protected)

// CONFIG5H
#pragma config CPB = OFF        // Boot Block Code Protection bit (Boot block (000000-0007FFh) not code-protected)
#pragma config CPD = OFF        // Data EEPROM Code Protection bit (Data EEPROM not code-protected)

// CONFIG6L
#pragma config WRT0 = OFF       // Write Protection bit (Block 0 (000800-001FFFh) not write-protected)
#pragma config WRT1 = OFF       // Write Protection bit (Block 1 (002000-003FFFh) not write-protected)
#pragma config WRT2 = OFF       // Write Protection bit (Block 2 (004000-005FFFh) not write-protected)
#pragma config WRT3 = OFF       // Write Protection bit (Block 3 (006000-007FFFh) not write-protected)

// CONFIG6H
#pragma config WRTC = OFF       // Configuration Register Write Protection bit (Configuration registers (300000-3000FFh) not write-protected)
#pragma config WRTB = OFF       // Boot Block Write Protection bit (Boot block (000000-0007FFh) not write-protected)
#pragma config WRTD = OFF       // Data EEPROM Write Protection bit (Data EEPROM not write-protected)

// CONFIG7L
#pragma config EBTR0 = OFF      // Table Read Protection bit (Block 0 (000800-001FFFh) not protected from table reads executed in other blocks)
#pragma config EBTR1 = OFF      // Table Read Protection bit (Block 1 (002000-003FFFh) not protected from table reads executed in other blocks)
#pragma config EBTR2 = OFF      // Table Read Protection bit (Block 2 (004000-005FFFh) not protected from table reads executed in other blocks)
#pragma config EBTR3 = OFF      // Table Read Protection bit (Block 3 (006000-007FFFh) not protected from table reads executed in other blocks)

// CONFIG7H
#pragma config EBTRB = OFF      // Boot Block Table Read Protection bit (Boot block (000000-0007FFh) not protected from table reads executed in other blocks)


#define delay1S  for(uint8_t i = 0 ; i < 20 ; i++) __delay_ms(50)

void main(){
    //Set LCD pins to output
    TRISD=0x00;
    
    //PIC2DEMPLUS related stuffs. Not needed on board without LCD power control and with R/W wired to ground
    LATDbits.LD7 = 1;           // LCD power on
    __delay_ms(50);             // LCD power up delay
    LATDbits.LD5 = 0;           // R/W set to W
    
    //LCD init
    LCDinit(LCD_INIT_CURSOR_DISABLED);
    __delay_ms(2);
    
    
    //Show a message
    LCDprintConst("Hello !");
    
    //Register a custom char
    const char testCar[]={0b00001010,0b00001010,0b00001010,0b00000000,0b00010001,0b00001110,0b00000000,0b00000000};
    LCDsetCustomCharConst(6,testCar);
    
    //Display it
    LCDpos(0,8);
    LCDuseCustomChar(6);
    
    //Show a progress bar and a value
    LCDsetupProgressBar();
    uint8_t test = 0;
    while(1){
        //Blanking value space and priting %
        LCDpos(0,12);
        LCDprintConst("   %");
        
        //Priting value
        LCDpos(0,14);
        LCDvalue8(test);
        
        //Printing progress bar
        LCDprogressBar(1,test,100);
        test++;
        if(test>100) {
            test = 0;
            delay1S;
        }
        __delay_ms(50);
    }
}
